package fi.vamk.fi.semdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/*just testing for now */
@RestController
public class Controller {
    @GetMapping("/")
    public String swagger() {
        return "redirect:/swagger-ui.html";
        // ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
    }

    @RequestMapping("/test")
    public String test() {
        return "{\"id\":\"test2\"}";
        // ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
    }
}